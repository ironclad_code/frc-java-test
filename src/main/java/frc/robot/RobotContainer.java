// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

//import frc.robot.commands.ElevatorCommand;
import frc.robot.commands.IntaketoggleCommand;
import frc.robot.commands.TankDriveCommand;
import frc.robot.commands.ToggleAccArmCommand;
import frc.robot.commands.ToggleStaticHooksCommand;
import frc.robot.subsystems.TankDriveSubsystem;
import frc.robot.subsystems.ClimbSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;

public class RobotContainer {  
  
  private TankDriveSubsystem TANK_DRIVE_SUBSYSTEM = new TankDriveSubsystem();
  private IntakeSubsystem INTAKE_SUBSYSTEM = new IntakeSubsystem();
  private Joystick AuxController = new Joystick(Constants.OperatorConstants.kAuxControllerPort);
  private Joystick DriverController = new Joystick(Constants.OperatorConstants.kDriverControllerPort);
  private ClimbSubsystem CLIMB_SUBSYSTEM = new ClimbSubsystem();
  
 
  public RobotContainer() {
    configureBindings();
    defaultCommands();
  }

  private void configureBindings() {
    
  }

  public void defaultCommands() {

    /* commented out so swerve drive will work__*/
    TANK_DRIVE_SUBSYSTEM.setDefaultCommand(new TankDriveCommand(TANK_DRIVE_SUBSYSTEM, DriverController));
    //CLIMB_SUBSYSTEM.setDefaultCommand(new ToggleStaticHooksCommand(CLIMB_SUBSYSTEM, DriverController));
    //INTAKE_SUBSYSTEM.setDefaultCommand(new IntaketoggleCommand( INTAKE_SUBSYSTEM, DriverController ));
    //CLIMB_SUBSYSTEM.setDefaultCommand(new ToggleAccArmCommand(CLIMB_SUBSYSTEM, DriverController));
  }


  public Command getAutonomousCommand() {
    return new SequentialCommandGroup();
  }
}
