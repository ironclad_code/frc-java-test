// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.TankDriveSubsystem;

public class TankDriveCommand extends CommandBase {

  private TankDriveSubsystem TANK_DRIVE_SUBSYSTEM;
  private final double DRIVE_LIMITER = 0.8; /*Change as needed, normally 0.8*/

  private Joystick DriverController; 
  

  public TankDriveCommand(TankDriveSubsystem TANK_DRIVE_SUBSYSTEM, Joystick DriverController) {
    this.TANK_DRIVE_SUBSYSTEM = TANK_DRIVE_SUBSYSTEM;
    this.DriverController = DriverController;
    

    this.addRequirements(this.TANK_DRIVE_SUBSYSTEM);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    TANK_DRIVE_SUBSYSTEM.set(DriverController.getRawAxis(1)* DRIVE_LIMITER, DriverController.getRawAxis(5)*DRIVE_LIMITER);
  }

  @Override
  public void end(boolean interrupted) {
    TANK_DRIVE_SUBSYSTEM.stop();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
