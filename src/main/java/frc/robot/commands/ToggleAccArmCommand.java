// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbSubsystem;

public class ToggleAccArmCommand extends CommandBase {
  private ClimbSubsystem ACC_ARM_SUBSYSTEM;
  private Joystick AuxController;
/*When Button B is presssed Switches Elevator Postion 
(Fully out Horizontally to Fully in and Vice Versa)
   * @param ACC_ARM_SUBSYSTEM <A subsystem that includes Setting and Toggling the Acc (Accuating) Arm>
   * @param AuxController <The Aux Controller>
   */
  /** Creates a new ToggleAccArmCommand. */
  public ToggleAccArmCommand(ClimbSubsystem ACC_ARM_SUBSYSTEM, Joystick AuxController ) {
    this.ACC_ARM_SUBSYSTEM = ACC_ARM_SUBSYSTEM;
    this.AuxController = AuxController;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(this.ACC_ARM_SUBSYSTEM);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (AuxController.getRawButton(2)) {
    ACC_ARM_SUBSYSTEM.toggleAccArm();
    }
    
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    ACC_ARM_SUBSYSTEM.setAccArm(false);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
