// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbSubsystem;

public class ToggleStaticHooksCommand extends CommandBase {
  private ClimbSubsystem STATIC_HOOKS_SUBSYSTEM;
  private Joystick AuxController;
/*When Button A is presssed Switches Static Hooks postion
   * @param STATIC_HOOKS_SUBSYSTEM <A subsystem that includes Setting and Toggling the Static Hooks>
   * @param AuxController <The Aux Controller>
   */
  /** Creates a new ToggleStaticHooksCommand. */
  public ToggleStaticHooksCommand(ClimbSubsystem STATIC_HOOKS_SUBSYSTEM, Joystick AuxCotroller) {
    this.STATIC_HOOKS_SUBSYSTEM = STATIC_HOOKS_SUBSYSTEM;
    this.AuxController = AuxCotroller;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(this.STATIC_HOOKS_SUBSYSTEM);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  
  @Override
  public void execute() {
    if (AuxController.getRawButton(1)) {
      STATIC_HOOKS_SUBSYSTEM.toggleStaticHooks();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    STATIC_HOOKS_SUBSYSTEM.setStaticHooks(false);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
