// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class IntaketoggleCommand extends CommandBase {
  private Joystick AuxController;
  private IntakeSubsystem INTAKE_SUBSYSTEM; //initilize in constructor
  
  /* <short explanation> Changes Intake to opposite postion (up to down, Down to up)
  *  @param INTAKE_SUBSYSTEM <A subsystem that activates the intake
   and includes both Setting and Toggling the Intake.>
  *  @param AuxController <The Aux Controller>
  * 
  */
  public IntaketoggleCommand(IntakeSubsystem INTAKE_SUBSYSTEM, Joystick AuxController) {
    this.INTAKE_SUBSYSTEM = INTAKE_SUBSYSTEM;
    this.AuxController = AuxController;
    addRequirements(this.INTAKE_SUBSYSTEM);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    if (AuxController.getRawButton(4)) {
      INTAKE_SUBSYSTEM.toggle();
    }
    
  }

  @Override
  public void end(boolean interrupted) {
    INTAKE_SUBSYSTEM.stop();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
