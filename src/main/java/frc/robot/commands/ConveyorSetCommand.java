// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ConveyorSubsystem;

public class ConveyorSetCommand extends CommandBase {
  /** Creates a new ConveyorSetCommand. */
  private double speed;
  private ConveyorSubsystem CONVEYOR_SUBSYSTEM;
  private boolean notFinished;

  public ConveyorSetCommand(ConveyorSubsystem CONVEYOR_SUBSYSTEM, double speed, boolean finished) {
    this.CONVEYOR_SUBSYSTEM = CONVEYOR_SUBSYSTEM;
    this.speed = speed;
    this.notFinished = !finished;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(this.CONVEYOR_SUBSYSTEM);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    CONVEYOR_SUBSYSTEM.set(speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    CONVEYOR_SUBSYSTEM.set(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return notFinished;
  }
}
