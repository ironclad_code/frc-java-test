// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ConveyorSubsystem;

public class ConveyorSortCommand extends CommandBase {
  /** Creates a new ConveyorSortCommand. */
  private boolean[] sensors;
  private ConveyorSubsystem CONVEYOR_SUBSYSTEM;
  
  private final double conveyorSpeed = 0.4;

  public ConveyorSortCommand(ConveyorSubsystem CONVEYOR_SUBSYSTEM) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.CONVEYOR_SUBSYSTEM = CONVEYOR_SUBSYSTEM;
    addRequirements(this.CONVEYOR_SUBSYSTEM);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
  sensors = CONVEYOR_SUBSYSTEM.getConveyorState();
  if(!sensors[2]&&sensors[0])
    CONVEYOR_SUBSYSTEM.set(conveyorSpeed);
  else
    CONVEYOR_SUBSYSTEM.set(0);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
