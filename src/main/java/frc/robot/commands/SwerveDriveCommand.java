// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Drivetrain;

public class SwerveDriveCommand extends CommandBase {
  /** Creates a new SwerveDriveCommand. */

  private Drivetrain SWERVE_DRIVE_SUBSYSTEM;

  private double xSpeed;
  private double ySpeed;
  private double rot;
  private boolean fieldRelative;

  public SwerveDriveCommand(Drivetrain SWERVE_DRIVE_SUBSYSTEM, double xSpeed, double ySpeed, double rot, boolean fieldRelative) {
    this.SWERVE_DRIVE_SUBSYSTEM = SWERVE_DRIVE_SUBSYSTEM;
    this.xSpeed = xSpeed;
    this.ySpeed = ySpeed;
    this.rot = rot;
    this.fieldRelative = fieldRelative;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(SWERVE_DRIVE_SUBSYSTEM);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    SWERVE_DRIVE_SUBSYSTEM.drive(xSpeed, ySpeed, rot, fieldRelative);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    SWERVE_DRIVE_SUBSYSTEM.drive(0, 0, 0, fieldRelative);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
