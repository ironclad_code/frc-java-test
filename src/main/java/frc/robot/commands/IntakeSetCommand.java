// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeSetCommand extends CommandBase {

  private IntakeSubsystem INTAKE_SUBSYSTEM;
  private boolean enabled;

  public IntakeSetCommand(IntakeSubsystem INTAKE_SUBSYSTEM, boolean enabled) {
    this.INTAKE_SUBSYSTEM = INTAKE_SUBSYSTEM;
    this.enabled = enabled;

    addRequirements(this.INTAKE_SUBSYSTEM);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    INTAKE_SUBSYSTEM.set(enabled);
  }

  @Override
  public void end(boolean interrupted) {}

  @Override
  public boolean isFinished() {
    return false;
  }
}
