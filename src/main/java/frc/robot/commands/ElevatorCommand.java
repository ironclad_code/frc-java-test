// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimbSubsystem;

public class ElevatorCommand extends CommandBase {
  private double speed;
  private ClimbSubsystem ELEVATOR_SUBSYSTEM;
  /** Creates a new ElevatorCommand. 
  public ElevatorCommand( double speed, ClimbSubsystem CLIMB_SUBSYSTEM) {
    this.speed = speed;
    this.ELEVATOR_SUBSYSTEM = CLIMB_SUBSYSTEM;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(this.ELEVATOR_SUBSYSTEM);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    ELEVATOR_SUBSYSTEM.setElevator(this.speed);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    ELEVATOR_SUBSYSTEM.setElevator(0);
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}*/
