// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

//test comment

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ClimbSubsystem extends SubsystemBase {
  //1 sol 2 motor 1 sol
 private Solenoid StaticHooks = new Solenoid(PneumaticsModuleType.CTREPCM, 3 );
 private Solenoid AccArm = new Solenoid(PneumaticsModuleType.CTREPCM, 0);
 private WPI_TalonFX elevatorMotorMain = new WPI_TalonFX(11);
 private WPI_TalonFX elevatorMotorFollow = new WPI_TalonFX(12);



 private boolean StaticHooksDown = false; 
 private boolean AccArmDown = false;


  /** Creates a new ClimbSubsystem. */
  public ClimbSubsystem() {
    
    elevatorMotorMain.setInverted(InvertType.None);
    elevatorMotorFollow.setInverted(InvertType.InvertMotorOutput);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
 public void toggleStaticHooks () {
  StaticHooksDown = !StaticHooksDown;

  if(StaticHooksDown) {
    StaticHooks.set(true);
  }
  else {
     StaticHooks.set(false);
  }
 }
 public void setStaticHooks (boolean setStaticHooks) {
  StaticHooksDown = setStaticHooks;
 StaticHooks.set(setStaticHooks);
 }

 public void toggleAccArm () {
  AccArmDown = !AccArmDown;

  if(AccArmDown) {
    AccArm.set(true);
  }
  else {
     AccArm.set(false);
  }
 }
 public void setAccArm (boolean setAccArm) {
  AccArmDown = setAccArm;
  AccArm.set (setAccArm);
 }
 /*public void setElevator (double speed) {
  elevatorMotorMain.set(ControlMode.PercentOutput, speed * 0.4);
 }*/
}
