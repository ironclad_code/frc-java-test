// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;


import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class IntakeSubsystem extends SubsystemBase {
  
  private WPI_TalonSRX intake = new WPI_TalonSRX(4);
  
  private Solenoid sol = new Solenoid(PneumaticsModuleType.CTREPCM, 1);
  private Boolean intakeDown = false;

  /** Creates a new Intake. */
  public IntakeSubsystem() {}

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void toggle() {
    intakeDown = !intakeDown;
  
    if(intakeDown) {
      intake.set(0.5);
      sol.set(true);
    } else {
      intake.set(0);
      sol.set(false);
    }
  }

  public void set(boolean set) {
    intakeDown = set;

    if(intakeDown) {
      intake.set(0.5);
      sol.set(true);
    } else {
      intake.set(0);
      sol.set(false);
    }
  }

  public void stop() {
    intakeDown = false;
    intake.set(0);
    sol.set(false);
  }
  
}
