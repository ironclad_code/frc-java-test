// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ConveyorSubsystem extends SubsystemBase {
  private WPI_TalonSRX conveyMotorMain = new WPI_TalonSRX(10);
  private WPI_TalonSRX conveyMotorFollow = new WPI_TalonSRX(9);
  private DigitalInput sensor1 = new DigitalInput(7);
  private DigitalInput sensor2 = new DigitalInput(8);
  private DigitalInput sensor3 = new DigitalInput(9);
  private boolean[] sensors = new boolean[3];
  /** Creates a new ConveyorSubsystem. */
  public ConveyorSubsystem() {
  conveyMotorFollow.follow(conveyMotorMain);

  }
  // sets both conveyor motors to the input speed
  public void set(double speed) {
    conveyMotorMain.set(ControlMode.PercentOutput, speed);
  }
  // returns an array of booleans for each ball location
  public boolean[] getConveyorState(){
    sensors[0] = sensor1.get();
    sensors[1] = sensor2.get();
    sensors[2] = sensor3.get();
    return sensors;
  }
  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
