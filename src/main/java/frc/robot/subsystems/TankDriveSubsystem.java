// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class TankDriveSubsystem extends SubsystemBase {

  private WPI_TalonFX rightMotorMain = new WPI_TalonFX(1);
  private WPI_TalonFX rightMotorFollow = new WPI_TalonFX(3);

  private WPI_TalonFX leftMotorMain = new WPI_TalonFX(14);
  private WPI_TalonFX leftMotorFollow = new WPI_TalonFX(2);

  /** Creates a new DriveTrain. */
  public TankDriveSubsystem() {
    leftMotorMain.configFactoryDefault();
    leftMotorFollow.configFactoryDefault();

    rightMotorMain.configFactoryDefault();
    rightMotorFollow.configFactoryDefault();

    leftMotorFollow.follow(this.leftMotorMain);
    rightMotorFollow.follow(this.rightMotorMain);

    leftMotorMain.setInverted(InvertType.None);
    leftMotorFollow.setInverted(InvertType.FollowMaster);

    rightMotorMain.setInverted(InvertType.InvertMotorOutput);
    rightMotorFollow.setInverted(InvertType.FollowMaster);

    rightMotorMain.configOpenloopRamp(0.2);
    leftMotorMain.configOpenloopRamp(0.2);

    

  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }

  public void set(double leftSpeed, double rightSpeed) {
    leftMotorMain.set(ControlMode.PercentOutput, leftSpeed);
    rightMotorMain.set(ControlMode.PercentOutput, rightSpeed);
  }

  public void stop() {
    leftMotorMain.set(ControlMode.PercentOutput, 0);
    rightMotorMain.set(ControlMode.PercentOutput, 0);
  }
}
